const pool = require('../../db');

class SudokuPuzzles {
  constructor(user_id, grid_type, solved_grid, puzzle_grid) {
    this.grid_type = grid_type;
    this.solved_grid = solved_grid;
    this.puzzle_grid = puzzle_grid;
    this.user_id = user_id;
  }

  async save() {
    try {
      const query = 'INSERT INTO sudoku_puzzles (user_id, grid_type, solved_grid, puzzle_grid) VALUES (?, ?, ?, ?)';
      const [results] = await pool.query(query, [this.user_id, this.grid_type, JSON.stringify(this.solved_grid), JSON.stringify(this.puzzle_grid)]);
      console.log('Sudoku puzzles inserted into database');
      return results;
    } catch (error) {
      console.error('Error inserting Sudoku puzzle into database:', error);
      throw error;
    }
  }
  
  static async findActivePuzzle(userId) { 
    try {
      const query = 'SELECT * FROM sudoku_puzzles WHERE user_id = ? AND status = ?';
      const [rows] = await pool.query(query, [userId, 'Active']);
      if (rows.length === 0) {
        throw new Error('no puzlle found');
      }
      return rows[0];
    } catch (error) {
      console.error('Error fetching puzlle from database:', error);
    }
  }

  static async updateScore(puzzleId, value) {
    try {
      const query = 'UPDATE sudoku_puzzles SET score = ? WHERE id = ?';
      const [results] = await pool.query(query, [value, puzzleId]);
      console.log('Puzzle grid updated in database');
      return value;
    } catch (error) {
      console.error('Error updating puzzle grid in database:', error);
      throw error;
    }
  }
  static async finishStatus(puzzleId) {
    try {
      const query = 'UPDATE sudoku_puzzles SET status = ? WHERE id = ?';
      const [results] = await pool.query(query, ["Finished", puzzleId]);
      console.log('Puzzle grid finished succcfuly in database');
      return puzzleGrid;
    } catch (error) {
      console.error('Error updating puzzle grid time in database:', error);
      throw error;
    }

  }
  static async updateEndTime(puzzleId , date) {
    try {
      const query = 'UPDATE sudoku_puzzles SET end_at = ? WHERE id = ?';
      const [results] = await pool.query(query, [date, puzzleId]);
      console.log('Puzzle grid finished succcfuly in database');
      return puzzleGrid;
    } catch (error) {
      console.error('Error updating puzzle grid time in database:', error);
      throw error;
    }

  }
  static async updateSolvedGrid(puzzleId, puzzleGrid) {
    try {
      const query = 'UPDATE sudoku_puzzles SET puzzle_grid = ? WHERE id = ?';
      const [results] = await pool.query(query, [JSON.stringify(puzzleGrid), puzzleId]);
      console.log('Puzzle grid updated in database');
      return puzzleGrid;
    } catch (error) {
      console.error('Error updating puzzle grid in database:', error);
      throw error;
    }
  }
  
  static async findById(puzzleId) { 
    try {
      const query = 'SELECT * FROM sudoku_puzzles WHERE id = ?';
      const [rows] = await pool.query(query, [puzzleId]);
      if (rows.length === 0 || !rows[0].solved_grid) {
        throw new Error('Grid not found');
      }
      return rows[0];
    } catch (error) {
      console.error('Error fetching Sudoku puzzle from database:', error);
      throw error;
    }
  }
  
  
}

module.exports = SudokuPuzzles;
