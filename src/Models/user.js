const pool = require('../../db');

class User {
  constructor(username, score) {
    this.username = username;
  }

  
  
  static async findUserName(username) { 
    try {
      const query = 'SELECT * FROM users WHERE username = ?';
      const [rows] = await pool.query(query, [username]);
      if (rows.length === 0) {
        throw new Error('no user found');
      }
      return rows[0];
    } catch (error) {
      console.error('Error fetching users from database:', error);
    }
  }

  static async save(username) {
    try {
      const query = 'INSERT INTO users (username) VALUES (?)';
      const [results] = await pool.query(query, [username]);
      console.log('user Created');
      return results;
    } catch (error) {
      console.error('Error inserting user puzzle into database:', error);
      throw error;
    }
  }

  static async updateScore(puzzleId, value) {
    try {
      const query = 'UPDATE sudoku_puzzles SET score = ? WHERE id = ?';
      const [results] = await pool.query(query, [value, puzzleId]);
      console.log('Puzzle grid updated in database');
      return value;
    } catch (error) {
      console.error('Error updating puzzle grid in database:', error);
      throw error;
    }
  }
  static async updateEndTime(puzzleId , date) {
    try {
      const query = 'UPDATE sudoku_puzzles SET end_at = ? WHERE id = ?';
      const [results] = await pool.query(query, [date, puzzleId]);
      console.log('Puzzle grid finished succcfuly in database');
      return puzzleGrid;
    } catch (error) {
      console.error('Error updating puzzle grid time in database:', error);
      throw error;
    }

  }
  static async updateSolvedGrid(puzzleId, puzzleGrid) {
    try {
      const query = 'UPDATE sudoku_puzzles SET puzzle_grid = ? WHERE id = ?';
      const [results] = await pool.query(query, [JSON.stringify(puzzleGrid), puzzleId]);
      console.log('Puzzle grid updated in database');
      return puzzleGrid;
    } catch (error) {
      console.error('Error updating puzzle grid in database:', error);
      throw error;
    }
  }
  
  
  
  
}

module.exports = User;
