const express = require('express');
const router = express.Router();
const userController = require('../Controllers/userController');
const usernameValidation = require('../Validation/usernameValidation');

router.post('/login',usernameValidation,  userController.login);

router.post('/signup', usernameValidation, userController.signup);



module.exports = router;