// Importing required modules
const express = require('express'); // Express.js framework for web applications
const router = express.Router(); // Router middleware for defining routes
const puzlleController = require('../Controllers/puzzleController'); // Controller functions for puzzle-related operations
const fillCellValidateInputs = require('../Validation/fillCellValidation'); // Middleware for validating fill cell inputs

// POST endpoint to generate a new puzzle
router.post('/create', puzlleController.generatePuzlle);

// POST endpoint to fill a cell in the puzzle
router.post('/fill-cell', fillCellValidateInputs, puzlleController.fillCell);

// GET endpoint to get a puzzle by its ID
router.get('/get-puzlle/:id', puzlleController.getPuzllById);

// Exporting the router module to be used in the main application
module.exports = router;
