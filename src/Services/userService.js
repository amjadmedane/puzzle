const User = require('../Models/user');
const SudokuPuzzles = require('../Models/sudokuPuzzles');


async function login(username)
  {
   
    const userData = await User.findUserName(username);
    return userData;
  
  }

  async function getActiveGame(userId)
  {
   
    const puzzle = await SudokuPuzzles.findActivePuzzle(userId);
    const { solved_grid, ...puzzleData } = puzzle;
    return puzzleData;
  
  }
async function signup(username) {

const userData = await User.findUserName(username);
if (!userData)
{
  const userData = await User.save(username);
  return userData
}

else{
  throw new Error('user Allredy Exist');
}

  }




  module.exports = {
    login,
    signup,
    getActiveGame,

  };