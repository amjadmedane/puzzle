const Puzzle = require('../Models/sudokuPuzzles');

async function generatePuzlle(userID, difficultyLevel) {
    
  // Generate solved and puzzle grids
    const { solvedGrid, puzzleGrid } = generateSudoku(difficultyLevel);

 // Create a new puzzle object
  
 const newPuzzle = new Puzzle(userID, difficultyLevel == "easy" ? 1 : 2, solvedGrid, puzzleGrid);
 // Save the new puzzle to the database   
 await newPuzzle.save();
    return newPuzzle;
  }
  function generateSudoku(difficultyLevel) {
    const solvedGrid = [];
    const puzzleGrid = [];
  
    // Generate solved grid
    for (let i = 0; i < 9; i++) {
        solvedGrid.push([]);
        puzzleGrid.push([]);
        for (let j = 0; j < 9; j++) {
            solvedGrid[i][j] = (i * 3 + Math.floor(i / 3) + j) % 9 + 1;
            puzzleGrid[i][j] = solvedGrid[i][j]; // Initially, puzzle grid is same as solved grid
        }
    }
  
    // Generate random list of cell positions
    const positions = Array.from({ length: 81 }, (_, i) => i);
    const shuffledPositions = positions.sort(() => Math.random() - 0.5);
  
    // Remove cells from puzzle grid based on difficulty level
    let emptyCells;
    switch (difficultyLevel) {
        case 'easy':
            emptyCells = 40; // Adjust the number of initially filled cells for easy difficulty
            break;
        case 'medium':
            emptyCells = 50; // Adjust the number of initially filled cells for medium difficulty
            break;
        // case 'hard':
        //     emptyCells = 60; // Adjust the number of initially filled cells for hard difficulty
        //     break;
        default:
            emptyCells = 40; // Default to easy difficulty
    }
  
    // Set cells to 0 in puzzle grid based on shuffled positions
    for (let i = 0; i < emptyCells; i++) {
        const position = shuffledPositions[i];
        const row = Math.floor(position / 9);
        const col = position % 9;
        puzzleGrid[row][col] = 0;
    }
  
    return { solvedGrid, puzzleGrid };
  }
  
 async function checkFinish(Grid)
 {
  const now = new Date();
  const formattedDateTime = now.toISOString().slice(0, 19).replace('T', ' ');


      if (Grid.solved_grid == Grid.puzzle_grid )
      {
        const finishedAt = await Puzzle.updateEndTime(Grid.id,formattedDateTime);
        const finishStatus = await Puzzle.finishStatus(Grid.id);

      }
 }
  async function updateSolvedGrid(id, SolvedGrid )
  {
    const grid = await Puzzle.updateSolvedGrid(id, SolvedGrid);
    return grid  ;
  }
  async function getSolvedGridById(id)
  {
    const grid = await Puzzle.findById(id);
    const solvedGrid = JSON.parse(grid.solved_grid);
    return  solvedGrid;
  }
  async function getGridById(id)
  {
    const grid = await Puzzle.findById(id);
    
    // const solvedGrid = JSON.parse(grid.solved_grid);
    return  grid;
  }

  async function updateScore(id,grid_type,oldScore,value)
  {

    if (value ==="sub")
    {
      value = (-1 * grid_type *100)+ oldScore;
    }
    else{
      value =  (1 * grid_type *10)+oldScore;
    }
    const grid = await Puzzle.updateScore(id,value);
    
    // const solvedGrid = JSON.parse(grid.solved_grid);
    return  grid;
  }



  module.exports = {
    generatePuzlle,
    updateSolvedGrid,
    getSolvedGridById,
    getGridById,
    updateScore,
    checkFinish,

  };