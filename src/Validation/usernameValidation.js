function usernameValidation(req, res, next) {
    const { username } = req.body;
  

    if (!username || username.length < 5) {
      return res.status(400).json({ message: 'Username must be at least 5 characters long' });
    }
    next();
  }
  module.exports = usernameValidation;