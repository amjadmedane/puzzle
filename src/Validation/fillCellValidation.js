function fillCellValidateInputs(req, res, next) {
    const { id, row, col , num } = req.body;
  

    if ( row < 0 || row > 8 || col < 0 || col > 8 || num < 1 || num > 9) {
        return res.status(400).json({ error: 'Invalid parameters' });
      }

    if (row === undefined || col === undefined || num === undefined) {
      return res.status(400).json({ message: 'cannot send empty data' });
    }
    if (row === "" || col === "" || num === "") {
        return res.status(400).json({ message: 'cannot send empty data' });
      }
      if (row === null || col === null || num === null ){
        return res.status(400).json({ message: 'cannot send empty data' });
      }
    next();
  }
  module.exports = fillCellValidateInputs;