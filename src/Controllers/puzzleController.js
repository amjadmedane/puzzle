// Importing required modules
const puzzleService = require('../Services/puzzleService'); // Service functions for puzzle-related operations
const { successResponse, errorResponse } = require('../ApiResponse/ApiResponse'); // Response utility functions

// Controller function to get a puzzle by its ID
async function getPuzllById(req, res ,next) {
  try {
    // Extracting puzzle ID from request parameters
    const id = req.params.id;

    // Calling service function to fetch puzzle data by ID
    const Puzlle = await puzzleService.getGridById(id);

    // Extracting solved_grid from puzzle data and renaming to puzzleData
    const { solved_grid, ...puzzleData } = Puzlle;

    // Sending success response with puzzleData
    res.json(successResponse('Puzlle found successfully', { puzzleData }));
  } catch (error) {
    // Handling errors
    console.error(error);
    res.status(500).json(errorResponse('Internal server error'));
  }
}

// Controller function to fill a cell in the puzzle
async function fillCell(req, res) {
  try {
    // Extracting data from request body
    const { id, row, col, num } = req.body;

    // Fetching puzzle grid data by ID
    const Grid = await puzzleService.getGridById(id);

    // Parsing solved grid from puzzle data
    const solveGrid = JSON.parse(Grid.solved_grid);

    // Checking if the provided number matches the solution
    if (num != solveGrid[row][col]) {
      // If not, updating the score and sending response
      const newScore = await puzzleService.updateScore(id, Grid.grid_type, Grid.score, "sub");
      return res.json(successResponse('Cell not correct', { new_Score: newScore }));
    }

    // Parsing puzzle grid from puzzle data
    let puzzleGrid = JSON.parse(Grid.puzzle_grid);

    // Checking if the cell is already filled
    if (num == puzzleGrid[row][col]) {
      return res.status(500).json({ error: 'Internal server error' });
    }

    // Filling the cell with the provided number
    puzzleGrid[row][col] = num;

    // Updating the score
    await puzzleService.updateScore(id, Grid.grid_type, Grid.score, "add");

    // Updating the solved grid
    const newGrid = await puzzleService.updateSolvedGrid(id, solveGrid);

    // Checking if the puzzle is finished
    await puzzleService.checkFinish(Grid);

    // Sending success response with updated grid
    res.json(successResponse('Cell filled successfully', { newGrid }));
  } catch (error) {
    // Handling errors
    console.error('Error filling cell:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
}

// Controller function to generate a new puzzle
async function generatePuzlle(req, res) {
  try {
    // Extracting user ID and difficulty level from request body
    const { userID, difficultyLevel } = req.body;

    // Validating input
    if (!difficultyLevel) {
      return res.status(400).json({ error: 'Difficulty level is required' });
    }

    // Calling service function to generate puzzle
    const Puzlle = await puzzleService.generatePuzlle(userID, difficultyLevel);

    // Sending success response with generated puzzle
    res.json(successResponse('Puzlle generated successfully', { Puzlle }));
  } catch (error) {
    // Handling errors
    console.error(error);
    res.status(500).json(errorResponse('Internal server error'));
  }
}

// Exporting controller functions to be used in routes
module.exports = {
  generatePuzlle,
  fillCell,
  getPuzllById,
  // Other controller functions
};
