const userService = require('../Services/userService');
const { successResponse, errorResponse } = require('../ApiResponse/ApiResponse');

async function login(req, res ,next) {
  try {
    const { username } = req.body;
    const user = await userService.login(username);
    console.log("useruseruseruser",user)
    if (!user)
    {
     return res.status(400).json({ error: 'user nor found' });
    }
    const ActivePuzzle = await userService.getActiveGame(user.id);
    console.log("qweqwe",ActivePuzzle)
    res.json(successResponse('user loging in successfully', { user:user, ActivePuzzle:ActivePuzzle  }));;
  } catch (error) {
    console.error(error);
  }
}
async function signup(req, res ,next) {
  try {
    const { username } = req.body;
    const user = await userService.signup(username);
    res.json(successResponse('user signup in successfully', { user }));;
  } catch (error) {
    console.error(error);
  }
}

  module.exports = {
    login,
    signup,
  };