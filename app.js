// Importing required modules
const express = require('express'); // Express.js framework for web applications
const bodyParser = require('body-parser'); // Middleware to parse JSON request bodies
const puzzleRoutes = require('./src/Routes/puzzle'); // Routes for puzzle-related endpoints
const userRoutes = require('./src/Routes/user'); // Routes for user-related endpoints

// Creating an Express application
const app = express();

// Middleware to parse JSON request bodies
app.use(bodyParser.json());

// Mounting puzzle routes on '/puzzle' endpoint
app.use('/puzzle', puzzleRoutes);

// Mounting user routes on root endpoint '/'
app.use('/', userRoutes);

// Defining the port for the server to listen on
const PORT = process.env.PORT || 3000;

// Starting the server and listening on the defined port
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});
